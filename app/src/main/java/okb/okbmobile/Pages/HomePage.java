package okb.okbmobile.Pages;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import okb.okbmobile.Models.DatabaseHelper;
import okb.okbmobile.Models.MapLoader;
import okb.okbmobile.R;

public class HomePage extends Activity {

    private MapView mapView;
    private MapboxMap map;
    private ProgressBar progressBar;

    private SharedPreferences sPreferences;

    public static final String HOME_PAGE_PREFERENCES = "HOME_PAGE_PREFERENCES";
    public static final String STATUS_LOADING_MAPS   = "STATUS_LOADING_MAPS";

    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(getApplicationContext(), getString(R.string.mapbox_access_token));
        setContentView(R.layout.activity_home_page);

//        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mapView     = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                map = mapboxMap;
            }
        });

//        LoadMap();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void LoadMap() {
        sPreferences = getSharedPreferences(HOME_PAGE_PREFERENCES, MODE_PRIVATE);

        boolean statusLoadingMaps;
        MapLoader mapLoader = new MapLoader(this, mapView, progressBar);
        if(sPreferences.contains(STATUS_LOADING_MAPS)) {
            statusLoadingMaps = sPreferences.getBoolean(STATUS_LOADING_MAPS, false);

            if(!statusLoadingMaps)
                mapLoader.Load();
        } else {
            mapLoader.Load();
        }
    }
}
