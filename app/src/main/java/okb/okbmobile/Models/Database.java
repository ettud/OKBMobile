package okb.okbmobile.Models;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;

public class Database {

    private Activity activity;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    private SharedPreferences preferences;

    private static final String DB_PREFERENCES = "DB_PREFERENCES";
    private static final String CREATED_DB     = "CREATED_DB";

    private static boolean IS_CREATED = false;

    public Database(Activity activity) {
        this.activity = activity;
        databaseHelper = new DatabaseHelper(activity);

        preferences = activity.getSharedPreferences(DB_PREFERENCES, Context.MODE_PRIVATE);
        IS_CREATED = preferences.getBoolean(CREATED_DB, false);

        if(!IS_CREATED) {
            databaseHelper.createDatabase();
        }

        database = databaseHelper.open();
    }

}
