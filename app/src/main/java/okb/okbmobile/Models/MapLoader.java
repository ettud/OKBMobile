package okb.okbmobile.Models;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineRegionError;
import com.mapbox.mapboxsdk.offline.OfflineRegionStatus;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import okb.okbmobile.Pages.HomePage;
import okb.okbmobile.R;

public class MapLoader {

    private Context context;
    private MapView mapView;
    private ProgressBar progressBar;

    private OfflineManager offlineManager;

    public static final String JSON_CHARSET           = "UTF-8";
    public static final String JSON_FIELD_REGION_NAME = "FIELD_REGION_NAME";
    public static final String TAG                    = "LOAD MAP";

    //ссылка на получение координат
    //https://www.mapbox.com/help/offline-estimator/
    private static final LatLng SOUTHWEST = new LatLng(55.13784, 61.37623);
    private static final LatLng NORTHEAST = new LatLng(55.13517, 61.36835);


    public MapLoader(Context context, MapView mapView, ProgressBar progressBar) {
        this.context        = context;
        this.mapView        = mapView;
        this.progressBar    = progressBar;
    }

    public void Load() {
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                offlineManager = offlineManager.getInstance(context);

                LatLngBounds latLngBounds = new LatLngBounds.Builder()
                        .include(SOUTHWEST)
                        .include(NORTHEAST)
                        .build();

                OfflineTilePyramidRegionDefinition definition = new OfflineTilePyramidRegionDefinition(
                        mapboxMap.getStyleUrl(),
                        latLngBounds,
                        10,
                        20,
                        context.getResources().getDisplayMetrics().density);

                // Set the metadata
                byte[] metadata;
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(JSON_FIELD_REGION_NAME, "ЧОКБ");
                    String json = jsonObject.toString();
                    metadata = json.getBytes(JSON_CHARSET);

                } catch (Exception exception) {
                    Log.e(TAG, "Failed to encode metadata: " + exception.getMessage());
                    metadata = null;
                }

                CreateRegion(definition, metadata);
            }
        });
    }

    private void CreateRegion(OfflineTilePyramidRegionDefinition definition,
                              byte[] metadata) {
        offlineManager.createOfflineRegion(
                definition,
                metadata,
                new OfflineManager.CreateOfflineRegionCallback() {
                    @Override
                    public void onCreate(OfflineRegion offlineRegion) {
                        offlineRegion.setDownloadState(OfflineRegion.STATE_ACTIVE);

                        startProgress();


                        // Отслеживание программа скачивания и загрузки карты
                        offlineRegion.setObserver(new OfflineRegion.OfflineRegionObserver() {
                            @Override
                            public void onStatusChanged(OfflineRegionStatus status) {
                                if (status.isComplete()) {
                                    endProgress();
                                    Message.Show(context, "Complete");

                                    SaveStatusLoadingMap(true);
                                }
                            }

                            @Override
                            public void onError(OfflineRegionError error) {
                                // If an error occurs, print to logcat
                                Log.e(TAG, "onError reason: " + error.getReason());
                                Log.e(TAG, "onError message: " + error.getMessage());

                                SaveStatusLoadingMap(false);
                            }

                            @Override
                            public void mapboxTileCountLimitExceeded(long limit) {
                                // Notify if offline region exceeds maximum tile count
                                Log.e(TAG, "Mapbox tile count limit exceeded: " + limit);
                                SaveStatusLoadingMap(false);
                            }
                        });
                    }

                    @Override
                    public void onError(String error) {
                        Log.e(TAG, "Error: " + error);
                    }
                });
    }

    private void startProgress() {

        // Start and show the progress bar
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void endProgress() {

        progressBar.setIndeterminate(false);
        progressBar.setVisibility(View.GONE);
    }

    private void SaveStatusLoadingMap(boolean status) {
        SharedPreferences sPreferences = context.getSharedPreferences(
                HomePage.HOME_PAGE_PREFERENCES, context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sPreferences.edit();
        editor.putBoolean(HomePage.STATUS_LOADING_MAPS, status);
        editor.apply();
    }
}

