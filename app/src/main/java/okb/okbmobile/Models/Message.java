package okb.okbmobile.Models;

import android.content.Context;
import android.widget.Toast;

public final class Message {
    public static void Show(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }
}
